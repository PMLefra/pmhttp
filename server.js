const net = require("net");
const HTTPSocket = require("./socket");
const Request = require("./request");

class HTTPServer extends net.Server {
  constructor() {
    super();
    this.on("listening", () => {
      this.on('connection', socket => {
        Object.setPrototypeOf(socket, HTTPSocket.prototype);

        if (verbose) {
          console.log('\n--------------------------------------------');
          console.log('New connection');
          let address = this.address();
          console.log(`Server is listening at ${address.address}:${address.port} (${address.family})`);
          console.log(`REMOTE Socket is listening at ${socket.remoteAddress}:${socket.remotePort} (${socket.remoteFamily})`);
          console.log('--------------------------------------------\n');
        }

        socket.setEncoding('utf8');

        socket.setTimeout(800000, function() {
          console.log('Socket timed out');
        });

        socket.on('data', async data => {
          if (verbose) {
            console.log("-- RECEIVED FROM SOCKET --");
            console.log(data);
          } else if (!quiet) {
            console.log("\n-> " + data.split("\n")[0]);
          }

          const request = new Request(data);
          if (request.error) {
            await socket.sendError(request.error);
            return;
          }

          switch (request.method.toUpperCase()) {
            case "GET":
              await socket.responseGET(request);
              break;
            case "HEAD":
              await socket.responseHEAD(request);
              break;
          }
        });

        socket.on('error', function(error) {
          console.log('Socket error: ' + error);
        });

        socket.on('timeout', function() {
          if (verbose) console.log('Socket timed out!');
          socket.end('Timed out!');
        });

        socket.on('end', function() {
          if (verbose) console.log('\n-- Socket ended --');
        });

        socket.on('close', function(error) {
          if (verbose) {
            let bRead = socket.bytesRead;
            let bWrite = socket.bytesWritten;
            console.log('Bytes read: ' + bRead);
            console.log('Bytes written: ' + bWrite);
            console.log('-- Socket closed --');
          }
          if (error) {
            console.log('Socket was closed because of a transmission error');
          }
        });
      });

      // noinspection HttpUrlsUsage
      console.log(`-- SERVER LISTENING ON http://${address}:${port}, AT ${dispRootFolder} --`);

      this.on('close', function() {
        console.log('Server closed !');
      });
    });
  }
}

global.httpVersion = "1.1";
global.supportedMethods = ["GET", "HEAD"];

module.exports = HTTPServer;