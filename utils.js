const fs = require("fs");

const HTTPCodes = require("./json/codes.json");
const MimeTypes = require("./json/mime.json");

const IconFolder = fs.readFileSync("./img/folder.png").toString('base64');
const IconFile = fs.readFileSync("./img/file.png").toString('base64');

/**
 * Capitalizes the first letter of each word
 * @param {string} str
 * @returns {string}
 */
function capitalize(str) {
  let fstr = "";
  let toCapitalize = true;
  for (let char of str) {
    if (toCapitalize) {
      fstr += char.toUpperCase();
      toCapitalize = false;
    } else fstr += char;
    if (/[\s\-]/.test(char)) toCapitalize = true;
  }
  return fstr;
}

async function fsExists(path) {
  return Boolean(await fs.promises.stat(path).catch(() => {}));
}

function generateErrorPage(errorNumber) {
  const fstr = `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>${errorNumber} | PMHTTP</title></head>`;
  return fstr + `<body><h1> Error ${errorNumber}: ${HTTPCodes[errorNumber.toString()].substr(4)}</h1></body></html>`;
}

async function generateIndexPage(dirPath) {
  let httpPath = dirPath.replace(global.rootFolder, "");
  let fstr = `<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>Index of ${httpPath} | PMHTTP</title></head>`;
  fstr += `<body><h1>Index of ${httpPath}</h1><ul>`;
  let files = await fs.promises.readdir(dirPath);
  if (httpPath !== "/") fstr += `<li><img alt="Folder icon" src="data:image/png;base64, ${IconFolder}" width="14">&nbsp;<a href="${(httpPath + "..").replace(/\/\//g, '/')}">../</a></li>`;
  for (let file of files) {
    const stat = await fs.promises.stat(dirPath + file).catch(()=>{});
    if (!stat) break;
    let img = stat.isDirectory() ? IconFolder : IconFile;
    fstr += `<li><img alt="File or folder icon" src="data:image/png;base64, ${img}" width="14">`;
    fstr += `&nbsp;<a href="${(httpPath + file).replace(/\/\//g, '/')}">${file}</a></li>`;
  }
  return fstr + `</ul></body></html>`;
}

module.exports = { HTTPCodes, MimeTypes, IconFolder, IconFile, capitalize, fsExists, generateErrorPage, generateIndexPage};