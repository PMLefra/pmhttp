const net = require("net");
const fs = require("fs");
const utils = require("./utils");
const Response = require("./response");

class HTTPSocket extends net.Socket {
  /**
   * Sends an HTTP response
   * Header should not contain 'Content-Length', 'Server', 'Date' nor 'Connection' yet
   * @param {Response} response
   * @param {boolean} sendContent
   * @returns {Promise<void>}
   */
  async sendHTTP(response, sendContent = true) {
    response.headers["Server"] = "PMHTTP";
    response.headers["Connection"] = "close";
    response.headers["Date"] = new Date().toUTCString();

    const contentType = response.headers["Content-Type"];

    let appTxt = ["application/javascript", "application/json", "application/xml"];
    let isText = false;
    let contentLength = response.body.length;
    if (contentLength > 0) {
      isText = contentType.split("/")[0] === "text" || appTxt.includes(contentType)
      contentLength++; // Pour le saut de ligne final
    }

    response.headers["Content-Length"] = contentLength.toString();

    const toSend = response.toBuffer();
    this.write(toSend);
    this.end();

    if (verbose) {
      let logMessage = "-- SENT TO SOCKET --\n";
      logMessage += response.getHeaderString();
      if (isText) logMessage += response.body.toString();
      else logMessage += "[Binary data]";
      if (logMessage.length > 300) logMessage = logMessage.slice(0, 300) + "\n[...]";
      logMessage += "\n";
      console.log(logMessage);
    }
    else if (!quiet)
      console.log("<- " + toSend.toString().split("\n")[0]);
  }

  /**
   * Sends a default index page
   * @param {string} path
   * @param {boolean} noContent
   * @returns {Promise<void>}
   */
  async sendIndex(path, noContent = false) {
    let response = new Response();
    response.headers["Content-Type"] = "text/html";
    response.body = await utils.generateIndexPage(path);
    await this.sendHTTP(response, !noContent);
  }

  /**
   * Sends an error page
   * @param {boolean | string | Number} errorCode
   * @param {boolean} noContent
   * @returns {Promise<void>}
   */
  async sendError(errorCode, noContent = false) {
    if (!errorCode) return;
    let response = new Response();
    response.statusCode = errorCode;
    response.headers["Content-Type"] = "text/html";
    response.body = utils.generateErrorPage(errorCode);
    await this.sendHTTP(response, !noContent);
  }

  /**
   * Sends an HTTP redirect response
   * @param {string | Number} code
   * @param {string} location
   * @returns {Promise<void>}
   */
  async sendRedirect(code, location) {
    let response = new Response();
    response.statusCode = code;
    response.headers["Location"] = location;
    await this.sendHTTP(response);
  }

  /**
   * Sends a document through the socket
   * @param {string} path
   * @param stats
   * @param {boolean} noContent
   * @returns {Promise<void>}
   */
  async sendDocument(path, stats, noContent = false) {
    let response = new Response();
    let type = "text/plain"; // Par défaut

    // Extension -> type
    const regExtension = /.*(\.\w+)$/;
    if (regExtension.test(path))
      type = utils.MimeTypes[regExtension.exec(path)[1].toLowerCase()] || type;
    response.headers["Content-Type"] = type;

    // Last-Modified
    if (stats) {
      let mtime = stats.mtime.toUTCString();
      if (stats.lmtime && stats.lmtime === mtime) {
        response.statusCode = 304;
        noContent = true;
      } else response.headers["Last-Modified"] = mtime;
    }

    // noinspection EqualityComparisonWithCoercionJS
    if (response.statusCode == 200)
      response.body = await fs.promises.readFile(path);

    await this.sendHTTP(response, !noContent);
  }

  /**
   * @param {Request} request
   * @param {boolean} noContent
   * @returns {Promise<void>}
   */
  async responseGET(request, noContent = false) {
    let path = request.realPath;
    if (!path || typeof path != "string") throw "Can't respond to nothing!";

    let stats = await fs.promises.stat(path).catch(() => {});

    // If the file doesn't exist
    if (!stats) {
      await this.sendError(404, noContent);
      return;
    }

    // Redirects if the file exists and is a folder
    const endsWithSlash = path[path.length - 1] === '/';
    if (stats.isDirectory() && !endsWithSlash) {
      const redirectPath = encodeURI((path + "/").replace(rootFolder, "").replace(/\/\//g, '/'));
      await this.sendRedirect(302, redirectPath);
      return;
    }

    // Cas d'un dossier
    if (endsWithSlash) {
      if (await utils.fsExists(path + "index.php")) path += "index.php";
      else if (await utils.fsExists(path + "index.html")) path += "index.html";
      else if (await utils.fsExists(path + "index.htm")) path += "index.htm";
      else {
        await this.sendIndex(path);
        return;
      }
    }

    stats = await fs.promises.stat(path);
    stats.lmtime = request.headers["If-Modified-Since"];

    // Et on envoie quand tout va bien
    await this.sendDocument(path, stats, noContent);
  }

  /**
   * @param {Request} request
   * @returns {Promise<void>}
   */
  async responseHEAD(request) {
    await this.responseGET(request, true);
  }
}

module.exports = HTTPSocket;