let d = new Date();
let h = d.getHours();
let m = d.getMinutes();
if (h.toString().length < 2) h = "0" + h;
if (m.toString().length < 2) m = "0" + m;
document.getElementById("hour").innerText = `${h}h${m}`;