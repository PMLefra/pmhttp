const statusCodes = require("./json/codes.json");

class Response {
  statusCode = 200;
  headers = {};
  _body = Buffer.alloc(0);

  getHeaderString() {
    let headerStr = `HTTP/${httpVersion} ${statusCodes[this.statusCode]}` + "\n";
    for (let header in this.headers) {
      headerStr += `${header}: ${this.headers[header]}\n`;
    }
    return headerStr + "\n";
  }

  toBuffer() {
    let headerStr = this.getHeaderString();
    return Buffer.concat([Buffer.from(headerStr), this._body, Buffer.from("\n")]);
  }

  toString() {
    return this.toBuffer().toString();
  }

  get body() {
    return this._body;
  }

  /**
   * Setter for this._body
   * @param {string | Buffer} value
   */
  set body(value) {
    this._body = Buffer.from(value);
  }
}

module.exports = Response;