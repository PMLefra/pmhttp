const utils = require("./utils");

class Request {
  error = false;
  headers = {};
  method = "";
  path = "";
  realPath = "";
  httpVersion = "";

  constructor(data) {
    const dataStr = data.toString();
    const dataStrArray = dataStr.split("\n");

    // First line format check
    const reg = /^\s*([A-z]+)\s+(\/[\w\-\/.?=%,]*)(?:\s+HTTP\/(\d\.\d))?\s*/;
    if (!reg.test(dataStr)) {
      this.error = 400;
      return;
    }

    [this.method, this.path, this.httpVersion] = reg.exec(dataStr).splice(1);
    global.httpVersion = this.httpVersion;

    if (!supportedMethods.includes(this.method)) {
      this.error = 501;
      return;
    }

    // Parsing headers
    // We're assuming here that every line is a header line, which isn't always true:
    // a request body can be given
    for (let line of dataStrArray) {
      const splitLine = line.split(":");
      if (splitLine.length < 2) continue;
      const key = utils.capitalize(splitLine[0].toLowerCase());
      this.headers[key] = splitLine.slice(1).join(":").trim();
    }

    // Basic query / path separation
    this.path = this.path.split('?')[0];
    // Security: prevents the client from accessing external files (important!)
    this.path = this.path.split('/').filter(a => (a !== '.' && a !== '..')).join('/');
    this.realPath = rootFolder + decodeURI(this.path);
  }
}

module.exports = Request;