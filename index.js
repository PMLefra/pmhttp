const argv = require("yargs")
  .options({
    'host': {
      alias: 'a',
      type: 'string',
      description: 'The server\'s IP address which it will listen to for requests.',
      default: 'localhost'
    },
    'port': {
      alias: 'p',
      type: 'number',
      description: 'TCP port to listen to.',
      default: 8080
    },
    'folder': {
      alias: 't',
      type: 'string',
      description: 'Folder to serve.',
      default: '.'
    },
    'verbose': {
      alias: 'v',
      type: 'boolean',
      description: 'Enables verbose output, with all HTTP requests displayed, a preview of the request and response bodies, and information about incoming connections.',
      default: false
    },
    'quiet': {
      alias: 'q',
      type: 'boolean',
      description: 'Don\'t log requests and responses in the console, option useless if verbose is enabled.',
      default: false
    }
  }).argv;

global.address = argv.host;
global.port = argv.port;
global.dispRootFolder = argv.folder.replace(/\/$/, "");
global.rootFolder = require("path").resolve(dispRootFolder);
global.verbose = argv.verbose;
global.quiet = argv.quiet;

process.chdir(__dirname);

const server = new (require("./server"))(() => {});
server.listen(port, address);